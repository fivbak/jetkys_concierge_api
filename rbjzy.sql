/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 50510
Source Host           : localhost:3306
Source Database       : rbjzy

Target Server Type    : MYSQL
Target Server Version : 50510
File Encoding         : 65001

Date: 2014-09-23 08:27:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `app_advertising`
-- ----------------------------
DROP TABLE IF EXISTS `app_advertising`;
CREATE TABLE `app_advertising` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_pic_url` varchar(255) DEFAULT NULL,
  `is_showing` tinyint(11) DEFAULT NULL COMMENT '1为显示，0为不显示',
  `ad_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_advertising
-- ----------------------------

-- ----------------------------
-- Table structure for `app_area`
-- ----------------------------
DROP TABLE IF EXISTS `app_area`;
CREATE TABLE `app_area` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_area
-- ----------------------------
INSERT INTO `app_area` VALUES ('1', '北海道', null);
INSERT INTO `app_area` VALUES ('2', '青森県', null);
INSERT INTO `app_area` VALUES ('3', '秋田県', null);
INSERT INTO `app_area` VALUES ('4', '岩手県', null);
INSERT INTO `app_area` VALUES ('5', '山形県', null);
INSERT INTO `app_area` VALUES ('6', '宮城県', null);
INSERT INTO `app_area` VALUES ('7', '福島県', null);
INSERT INTO `app_area` VALUES ('8', '新潟県', null);
INSERT INTO `app_area` VALUES ('9', '富山県', null);
INSERT INTO `app_area` VALUES ('10', '石川県', null);
INSERT INTO `app_area` VALUES ('11', '群馬県', null);
INSERT INTO `app_area` VALUES ('12', '栃木県', null);
INSERT INTO `app_area` VALUES ('13', '長野県', null);
INSERT INTO `app_area` VALUES ('14', '岐阜県', null);
INSERT INTO `app_area` VALUES ('15', '埼玉県', null);
INSERT INTO `app_area` VALUES ('16', '茨城県', null);
INSERT INTO `app_area` VALUES ('17', '東京都', null);
INSERT INTO `app_area` VALUES ('18', '千葉県', null);
INSERT INTO `app_area` VALUES ('19', '神奈川県', null);
INSERT INTO `app_area` VALUES ('20', '静岡県', null);
INSERT INTO `app_area` VALUES ('21', '山梨県', null);
INSERT INTO `app_area` VALUES ('22', '愛知県', null);
INSERT INTO `app_area` VALUES ('23', '福井県', null);
INSERT INTO `app_area` VALUES ('24', '滋賀県', null);
INSERT INTO `app_area` VALUES ('25', '三重県', null);
INSERT INTO `app_area` VALUES ('26', '京都府', null);
INSERT INTO `app_area` VALUES ('27', '奈良県', null);
INSERT INTO `app_area` VALUES ('28', '兵庫県', null);
INSERT INTO `app_area` VALUES ('29', '大阪府', null);
INSERT INTO `app_area` VALUES ('30', '和歌山県', null);
INSERT INTO `app_area` VALUES ('31', '島根県', null);
INSERT INTO `app_area` VALUES ('32', '鳥取県', null);
INSERT INTO `app_area` VALUES ('33', '岡山県', null);
INSERT INTO `app_area` VALUES ('34', '広島県', null);
INSERT INTO `app_area` VALUES ('35', '山口県', null);
INSERT INTO `app_area` VALUES ('36', '香川県', null);
INSERT INTO `app_area` VALUES ('37', '愛媛県', null);
INSERT INTO `app_area` VALUES ('38', '徳島県', null);
INSERT INTO `app_area` VALUES ('39', '高知県', null);
INSERT INTO `app_area` VALUES ('40', '大分県', null);
INSERT INTO `app_area` VALUES ('41', '福岡県', null);
INSERT INTO `app_area` VALUES ('42', '佐賀県', null);
INSERT INTO `app_area` VALUES ('43', '熊本県', null);
INSERT INTO `app_area` VALUES ('44', '長崎県', null);
INSERT INTO `app_area` VALUES ('45', '宮崎県', null);
INSERT INTO `app_area` VALUES ('46', '鹿児島県', null);
INSERT INTO `app_area` VALUES ('47', '沖縄県', null);
INSERT INTO `app_area` VALUES ('48', '2', '3');
INSERT INTO `app_area` VALUES ('49', '2', '4');

-- ----------------------------
-- Table structure for `app_article`
-- ----------------------------
DROP TABLE IF EXISTS `app_article`;
CREATE TABLE `app_article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `article_title` varchar(255) DEFAULT NULL,
  `article_time` varchar(255) DEFAULT NULL,
  `article_content` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_article
-- ----------------------------

-- ----------------------------
-- Table structure for `app_attention`
-- ----------------------------
DROP TABLE IF EXISTS `app_attention`;
CREATE TABLE `app_attention` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `target_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1为是，2为否',
  `insert_time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_attention
-- ----------------------------

-- ----------------------------
-- Table structure for `app_buy_coins`
-- ----------------------------
DROP TABLE IF EXISTS `app_buy_coins`;
CREATE TABLE `app_buy_coins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `change_coins` int(11) DEFAULT NULL,
  `yen` int(11) DEFAULT NULL,
  `insert_time` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL COMMENT '1为成功，2为失败',
  `status` tinyint(11) DEFAULT NULL COMMENT '1为成功，2为失败',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_buy_coins
-- ----------------------------

-- ----------------------------
-- Table structure for `app_closure`
-- ----------------------------
DROP TABLE IF EXISTS `app_closure`;
CREATE TABLE `app_closure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `close_time` int(11) DEFAULT NULL,
  `insert_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_closure
-- ----------------------------
INSERT INTO `app_closure` VALUES ('4', 'ライフステージ', '201408', '1410913592');
INSERT INTO `app_closure` VALUES ('2', 'ジョイント・コーポレーション', '201407', '1410606651');
INSERT INTO `app_closure` VALUES ('5', '中央コーボレーション', '201408', '1410913810');
INSERT INTO `app_closure` VALUES ('10', '倒産企業１', '201409', '1410920835');
INSERT INTO `app_closure` VALUES ('11', '倒産企業２', '201409', '1410921683');
INSERT INTO `app_closure` VALUES ('12', '倒産企業３', '201409', '1410924661');
INSERT INTO `app_closure` VALUES ('13', '倒産企業４', '201409', '1410924680');
INSERT INTO `app_closure` VALUES ('14', '倒産企業５', '201409', '1410924705');
INSERT INTO `app_closure` VALUES ('15', '倒産企業６', '201409', '1410924718');
INSERT INTO `app_closure` VALUES ('16', '倒産企業７', '201409', '1410924762');
INSERT INTO `app_closure` VALUES ('17', '倒産企業８', '201408', '1410924818');

-- ----------------------------
-- Table structure for `app_closure_comments`
-- ----------------------------
DROP TABLE IF EXISTS `app_closure_comments`;
CREATE TABLE `app_closure_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `closure_id` int(11) DEFAULT NULL,
  `closure_time` int(255) DEFAULT NULL,
  `closure_content` text,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_closure_comments
-- ----------------------------

-- ----------------------------
-- Table structure for `app_closure_reply`
-- ----------------------------
DROP TABLE IF EXISTS `app_closure_reply`;
CREATE TABLE `app_closure_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `closure_id` int(11) DEFAULT NULL,
  `reply_content` text,
  `insert_time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_closure_reply
-- ----------------------------

-- ----------------------------
-- Table structure for `app_company`
-- ----------------------------
DROP TABLE IF EXISTS `app_company`;
CREATE TABLE `app_company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1' COMMENT '1为倒闭公司，2为现场公司',
  `insert_time` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_company
-- ----------------------------
INSERT INTO `app_company` VALUES ('1', '鹿島建設', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('2', '清水建設', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('3', '大成建設', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('4', '大林組', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('5', '竹中工務店', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('6', 'アーツ&クラフツ', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('7', 'アーレックス', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('8', 'アイサワ工業', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('9', 'アイジーコンサルティング', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('10', 'アイネットテクノ', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('11', 'アエラホーム', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('12', '青木あすなろ建設', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('13', '青木マリーン', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('14', 'あおみ建設', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('15', '暁飯島工業', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('16', '淺沼組', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('17', '旭化成ホームズ', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('18', '朝日工業社', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('19', '旭コムテク', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('20', '旭ホームズ', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('21', '味岡建設', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('22', 'アスト技建', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('23', '麻生フオームクリート', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('24', '安達建設', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('25', '穴吹工務店', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('26', '穴吹ミサワホーム', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('27', 'アライヴコミュニティ', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('28', '新井組', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('29', '荒井建設', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('30', 'アルス製作所', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('31', '安藤建設', '2', '1411129271', '1');
INSERT INTO `app_company` VALUES ('32', '安藤?間', '2', '1411129271', '2');
INSERT INTO `app_company` VALUES ('33', '石橋工業(栃木県)', '2', '1411129271', '2');
INSERT INTO `app_company` VALUES ('34', '石橋工業(長崎県)', '2', '1411129271', '2');
INSERT INTO `app_company` VALUES ('35', '石橋建設工業(福島県)', '2', '1411129271', '2');
INSERT INTO `app_company` VALUES ('36', '石橋建設工業(群馬県)', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('37', '伊田テクノス', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('38', '一条工務店', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('39', '一条工務店群馬', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('40', '一条工務店山陰', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('41', '一条工務店仙台', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('42', '一条工務店宮城', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('43', 'イチケン', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('44', '伊藤組土建', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('45', '犬飼建設', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('46', '井上工業', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('47', 'イビデングリーンテック', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('48', 'イブリアスコン', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('49', 'イワクラホーム', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('50', '岩田地崎建設', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('51', '岩舘電気', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('52', '岩永組', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('53', 'インターライフホールディングス', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('54', '日商インターライフ', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('55', '植木組', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('56', 'ウエストホールディングス', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('57', 'ウエスト', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('58', '宇部興産機械', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('59', '上村建設', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('60', 'ウベハウス', '2', '1411129272', '2');
INSERT INTO `app_company` VALUES ('61', '梅林建設', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('62', 'ASS', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('63', 'AS-SZKi', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('64', 'A.Cホールディングス', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('65', 'エス?バイ?エル', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('66', 'エステート住宅産業', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('67', 'NDS', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('68', 'NTTファシリティーズ', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('69', 'エム?テック', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('70', 'エムビーエス', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('71', '荏原建設', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('72', '王子木材緑化', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('73', '大木建設', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('74', '大野建設', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('75', 'オーテック', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('76', '大林組', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('77', '大林道路', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('78', '大本組', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('79', '大盛工業', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('80', '岡田組', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('81', '沖ウィンテック', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('82', '沖電工', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('83', '奥アンツーカ', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('84', '奥村組', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('85', '奥村組土木興業', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('86', '重川（おもかわ）材木店', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('87', '長田組土木', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('88', '小田急建設', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('89', '小田急ハウジング', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('90', 'オーティエンジニアリング', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('91', 'オリエンタル建設', '2', '1411129272', '3');
INSERT INTO `app_company` VALUES ('92', 'オリエンタル白石', '2', '1411129272', '3');

-- ----------------------------
-- Table structure for `app_consultation`
-- ----------------------------
DROP TABLE IF EXISTS `app_consultation`;
CREATE TABLE `app_consultation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `con_content` text,
  `insert_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_consultation
-- ----------------------------

-- ----------------------------
-- Table structure for `app_consultation_type`
-- ----------------------------
DROP TABLE IF EXISTS `app_consultation_type`;
CREATE TABLE `app_consultation_type` (
  `con_id` int(11) NOT NULL,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`con_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_consultation_type
-- ----------------------------

-- ----------------------------
-- Table structure for `app_evaluate`
-- ----------------------------
DROP TABLE IF EXISTS `app_evaluate`;
CREATE TABLE `app_evaluate` (
  `id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `evaluation` text COMMENT '评价',
  `evaluation_time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_evaluate
-- ----------------------------

-- ----------------------------
-- Table structure for `app_freetime`
-- ----------------------------
DROP TABLE IF EXISTS `app_freetime`;
CREATE TABLE `app_freetime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `start_time` varchar(255) DEFAULT NULL,
  `end_time` varchar(255) DEFAULT NULL,
  `people` varchar(255) DEFAULT NULL,
  `area_ids` text,
  `introduction` text,
  `insert_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_freetime
-- ----------------------------
INSERT INTO `app_freetime` VALUES ('1', '55', null, '5', '2014-5-8', '2014-9-20', '3', '3', '1', '1411117277');

-- ----------------------------
-- Table structure for `app_gold`
-- ----------------------------
DROP TABLE IF EXISTS `app_gold`;
CREATE TABLE `app_gold` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `start_coins` int(11) DEFAULT NULL COMMENT '起初金币数量',
  `change_coins` int(11) DEFAULT NULL COMMENT '变化数量',
  `end_coins` int(11) DEFAULT NULL COMMENT '最终金币数量',
  `insert_time` int(11) DEFAULT NULL,
  `introduction` text COMMENT '详情描述',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_gold
-- ----------------------------

-- ----------------------------
-- Table structure for `app_jobs`
-- ----------------------------
DROP TABLE IF EXISTS `app_jobs`;
CREATE TABLE `app_jobs` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for `app_lottery`
-- ----------------------------
DROP TABLE IF EXISTS `app_lottery`;
CREATE TABLE `app_lottery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `insert_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_lottery
-- ----------------------------

-- ----------------------------
-- Table structure for `app_lottery_record`
-- ----------------------------
DROP TABLE IF EXISTS `app_lottery_record`;
CREATE TABLE `app_lottery_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insert_time` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_lottery_record
-- ----------------------------

-- ----------------------------
-- Table structure for `app_member`
-- ----------------------------
DROP TABLE IF EXISTS `app_member`;
CREATE TABLE `app_member` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_token` varchar(255) DEFAULT NULL,
  `user_nick` varchar(50) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_pwd` varchar(255) DEFAULT NULL,
  `user_sex` tinyint(4) DEFAULT NULL COMMENT '1男，2女',
  `user_birth` varchar(255) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `distrcit` varchar(255) DEFAULT NULL,
  `insert_time` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '0:正常，1:Black，2:退会',
  `user_introduction` text,
  `cost` varchar(255) DEFAULT NULL,
  `blood` varchar(255) DEFAULT NULL,
  `health` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_member
-- ----------------------------
INSERT INTO `app_member` VALUES ('6', '88673186b8593fe662b775f6cfb14621', '好好干活', 'lixiaoyong@hotm.com', '123456', '2', '1909-11', '1', '和平', '1410606651', '0', null, null, null, null);
INSERT INTO `app_member` VALUES ('7', 'e41a59786533d4ae1fc44378da6b8d98', 'Dfsdf', '123@qq.com', '123456', '1', '2014-09-19', '1', 'Sdfsasdf', '1411107325', '0', null, null, null, null);
INSERT INTO `app_member` VALUES ('8', 'fe8ca749263044f59c89c5aac17631bb', 'Zhangsan', 'zhangsan@qq.com', '123456', '1', '2014-09-19', '1', 'Afdfadsf', '1411109186', '0', null, null, null, null);
INSERT INTO `app_member` VALUES ('9', '379aefee233b07279956d0f537bd0032', 'Zhangsan', '1234@qq.com', '123456', '1', '2014-09-19', '1', 'Ffguiu', '1411109457', '0', null, null, null, null);
INSERT INTO `app_member` VALUES ('10', 'faaf276419a537ca4e798a0dd0927398', 'yangyi', 'yangyi@vicgoo.com', '123654', '1', '1989-10-10', '1', '2', '1411113703', '0', null, null, null, null);
INSERT INTO `app_member` VALUES ('11', 'a792dc31e7c96d8036e2bfb491543fdb', 'yangyi', 'yangyi@vicgoo.com', '123456', '1', '1989-10-10', '1', '2', '1411113780', '0', null, null, null, null);
INSERT INTO `app_member` VALUES ('12', '3f4168f794f2ece554df50db599df7fb', 'yangyi', 'yangyi@vicgoo.com', '123456', '1', '1989-10-10', '1', '2', '1411115002', '0', null, null, null, null);
INSERT INTO `app_member` VALUES ('13', '8f5d0980589a69af33b89ea40a935dfe', 'yangyi', 'yangyi@vicgoo.com', '123456', '1', '1989-10-10', '1', '2', '1411115234', '0', null, null, null, null);
INSERT INTO `app_member` VALUES ('14', '54f1a3b5ef458219653e3de88af96ad0', 'yangyi', 'yangyi@vicgoo.com', '123456', '1', '1989-10-10', '1', '2', '1411115264', '0', null, null, null, null);
INSERT INTO `app_member` VALUES ('15', '3b3618e287e15aedb53ee4587d066405', 'yangyi', 'yangyi@vicgoo.com', '123456', '1', '1989-10-10', '1', '2', '1411115360', '0', null, null, null, null);
INSERT INTO `app_member` VALUES ('16', '1519c6fc4efcc591102f0d6a2f8a6640', 'wsy', 'nicoal@vicgoo.com', '321654', '2', '1989-5-5', '25', '4', '1411194984', '0', null, null, null, null);
INSERT INTO `app_member` VALUES ('17', 'e936c5b9cb0a8ff4420b3516377bf6c2', '杨益', '491966656@qq.com', '123456', '1', '1989-12-26', '2', '东京区', '1411198673', '0', null, null, null, null);

-- ----------------------------
-- Table structure for `app_member_appraisal`
-- ----------------------------
DROP TABLE IF EXISTS `app_member_appraisal`;
CREATE TABLE `app_member_appraisal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `target_id` int(11) DEFAULT NULL COMMENT '被评价人的user_id',
  `rate_score` int(11) DEFAULT NULL,
  `insert_time` int(11) DEFAULT NULL,
  `rate_content` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_member_appraisal
-- ----------------------------
INSERT INTO `app_member_appraisal` VALUES ('1', '10', '0', '2', '1411119687', '啦');

-- ----------------------------
-- Table structure for `app_member_certificates`
-- ----------------------------
DROP TABLE IF EXISTS `app_member_certificates`;
CREATE TABLE `app_member_certificates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `certificates` varchar(255) DEFAULT NULL,
  `insert_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_member_certificates
-- ----------------------------

-- ----------------------------
-- Table structure for `app_member_chat`
-- ----------------------------
DROP TABLE IF EXISTS `app_member_chat`;
CREATE TABLE `app_member_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `content` text,
  `f_id` int(11) DEFAULT NULL,
  `insert_time` int(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1' COMMENT '1为文字信息，2为发注书',
  `pic` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '1为已读，2为未读',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_member_chat
-- ----------------------------
INSERT INTO `app_member_chat` VALUES ('4', '10', '啦', null, '1411119646', '0', null, '1');

-- ----------------------------
-- Table structure for `app_member_friend`
-- ----------------------------
DROP TABLE IF EXISTS `app_member_friend`;
CREATE TABLE `app_member_friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `friend_id` int(11) DEFAULT NULL,
  `insert_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_member_friend
-- ----------------------------

-- ----------------------------
-- Table structure for `app_member_job`
-- ----------------------------
DROP TABLE IF EXISTS `app_member_job`;
CREATE TABLE `app_member_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_job` varchar(255) DEFAULT NULL,
  `insert_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_member_job
-- ----------------------------

-- ----------------------------
-- Table structure for `app_menu`
-- ----------------------------
DROP TABLE IF EXISTS `app_menu`;
CREATE TABLE `app_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `priority` varchar(255) DEFAULT NULL,
  `menu_name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '1为显示，0为否',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_menu
-- ----------------------------
INSERT INTO `app_menu` VALUES ('1', '0', '回せ', '1');
INSERT INTO `app_menu` VALUES ('2', '1', '速報!倒産情報!!', '1');
INSERT INTO `app_menu` VALUES ('3', '2', '現場の掃き溜め', '1');
INSERT INTO `app_menu` VALUES ('4', '3', '俺のシリーズ', '1');
INSERT INTO `app_menu` VALUES ('5', '4', '現パラニュース', '1');
INSERT INTO `app_menu` VALUES ('6', '5', '現パラスボーツ', '1');
INSERT INTO `app_menu` VALUES ('7', '6', '現場の貸し借り', '1');
INSERT INTO `app_menu` VALUES ('8', '7', '売店', '1');
INSERT INTO `app_menu` VALUES ('9', '8', 'マイページ', '1');
INSERT INTO `app_menu` VALUES ('10', '9', '便所', '1');

-- ----------------------------
-- Table structure for `app_message_box`
-- ----------------------------
DROP TABLE IF EXISTS `app_message_box`;
CREATE TABLE `app_message_box` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_nick` varchar(255) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1为未读，2为已读，3为已回复,4为已发送',
  `message` varchar(255) DEFAULT NULL,
  `insert_time` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_message_box
-- ----------------------------

-- ----------------------------
-- Table structure for `app_mock`
-- ----------------------------
DROP TABLE IF EXISTS `app_mock`;
CREATE TABLE `app_mock` (
  `mock_id` int(11) NOT NULL AUTO_INCREMENT,
  `workp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `mock_time` int(11) DEFAULT NULL,
  `mock_content` text,
  PRIMARY KEY (`mock_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_mock
-- ----------------------------

-- ----------------------------
-- Table structure for `app_mock_reply`
-- ----------------------------
DROP TABLE IF EXISTS `app_mock_reply`;
CREATE TABLE `app_mock_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `mock_id` int(11) DEFAULT NULL,
  `insert_time` int(11) DEFAULT NULL,
  `reply_content` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_mock_reply
-- ----------------------------
INSERT INTO `app_mock_reply` VALUES ('1', '2', '44', '1411116655', '啦啦');

-- ----------------------------
-- Table structure for `app_my_series`
-- ----------------------------
DROP TABLE IF EXISTS `app_my_series`;
CREATE TABLE `app_my_series` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `insert_time` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_my_series
-- ----------------------------

-- ----------------------------
-- Table structure for `app_my_series_info`
-- ----------------------------
DROP TABLE IF EXISTS `app_my_series_info`;
CREATE TABLE `app_my_series_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `insert_time` int(11) DEFAULT NULL,
  `content` text,
  `pic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_my_series_info
-- ----------------------------

-- ----------------------------
-- Table structure for `app_news_category`
-- ----------------------------
DROP TABLE IF EXISTS `app_news_category`;
CREATE TABLE `app_news_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  `insert_time` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_news_category
-- ----------------------------

-- ----------------------------
-- Table structure for `app_news_info`
-- ----------------------------
DROP TABLE IF EXISTS `app_news_info`;
CREATE TABLE `app_news_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `insert_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_news_info
-- ----------------------------

-- ----------------------------
-- Table structure for `app_news_two_category`
-- ----------------------------
DROP TABLE IF EXISTS `app_news_two_category`;
CREATE TABLE `app_news_two_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  `insert_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_news_two_category
-- ----------------------------

-- ----------------------------
-- Table structure for `app_news_two_info`
-- ----------------------------
DROP TABLE IF EXISTS `app_news_two_info`;
CREATE TABLE `app_news_two_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `pic` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `insert_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_news_two_info
-- ----------------------------

-- ----------------------------
-- Table structure for `app_notes`
-- ----------------------------
DROP TABLE IF EXISTS `app_notes`;
CREATE TABLE `app_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `insert_time` int(11) DEFAULT NULL,
  `content` text,
  `type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_notes
-- ----------------------------

-- ----------------------------
-- Table structure for `app_notes_type`
-- ----------------------------
DROP TABLE IF EXISTS `app_notes_type`;
CREATE TABLE `app_notes_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_notes_type
-- ----------------------------

-- ----------------------------
-- Table structure for `app_order`
-- ----------------------------
DROP TABLE IF EXISTS `app_order`;
CREATE TABLE `app_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '下单人id',
  `title` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '下单人',
  `home` varchar(255) DEFAULT NULL,
  `f_id` int(11) DEFAULT NULL,
  `user_name` varchar(20) DEFAULT NULL COMMENT '接单人',
  `matter` varchar(255) DEFAULT NULL COMMENT '注意事项',
  `status` tinyint(4) DEFAULT '1' COMMENT '1已发送，2已接受，3已确认',
  `confirm_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_order
-- ----------------------------

-- ----------------------------
-- Table structure for `app_product`
-- ----------------------------
DROP TABLE IF EXISTS `app_product`;
CREATE TABLE `app_product` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `coins` varchar(255) DEFAULT NULL,
  `fee` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_product
-- ----------------------------

-- ----------------------------
-- Table structure for `app_scene`
-- ----------------------------
DROP TABLE IF EXISTS `app_scene`;
CREATE TABLE `app_scene` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `people` varchar(255) DEFAULT NULL,
  `start_time` varchar(255) DEFAULT NULL,
  `end_time` varchar(255) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `content` text,
  `insert_time` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_scene
-- ----------------------------
INSERT INTO `app_scene` VALUES ('1', '啦', '5', '啦', '1368547200', '1402761600', '4', '1', '1411117674', '2');

-- ----------------------------
-- Table structure for `app_switch`
-- ----------------------------
DROP TABLE IF EXISTS `app_switch`;
CREATE TABLE `app_switch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `setting_list` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1为开，0为关',
  `insert_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_switch
-- ----------------------------

-- ----------------------------
-- Table structure for `app_workp`
-- ----------------------------
DROP TABLE IF EXISTS `app_workp`;
CREATE TABLE `app_workp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `insert_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of app_workp
-- ----------------------------
INSERT INTO `app_workp` VALUES ('1', 'オハナ', '2', '44', '2', '1411116736');
INSERT INTO `app_workp` VALUES ('2', 'ハイコート', '2', '44', '2', '1411116750');
INSERT INTO `app_workp` VALUES ('3', 'グレーシア', '2', '4', null, '1411117674');

-- ----------------------------
-- Table structure for `gen_user`
-- ----------------------------
DROP TABLE IF EXISTS `gen_user`;
CREATE TABLE `gen_user` (
  `user_id` varchar(16) NOT NULL,
  `user_token` varchar(20) NOT NULL,
  `user_nick` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_pwd` varchar(15) NOT NULL,
  `user_sex` varchar(1) NOT NULL,
  `user_birth` date NOT NULL,
  `city_id` varchar(2) NOT NULL,
  `dist` varchar(50) NOT NULL,
  `user_sts` varchar(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gen_user
-- ----------------------------
INSERT INTO `gen_user` VALUES ('M000000000000001', 'T0000000000000000001', 'テストユーザー１', 'fivbak@gmail.com', '123456', '1', '2014-09-04', '19', '横浜市西区みなとみらい2-2-1 ランドマークタワー12F', '0');
INSERT INTO `gen_user` VALUES ('M000000000000002', 'T0000000000000000002', 'テストユーザー2', 'fivbak@gmail.com', '123456', '1', '2014-09-05', '19', '横浜市西区みなとみらい2-2-1 ランドマークタワー11F', '0');
