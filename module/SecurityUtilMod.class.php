<?php
// App::uses('Constant', 'Lib');
class SecurityUtilMod extends commonMod {
	public function __construct() {
		parent::__construct ();
	}
	/**
	 * 時刻チェック処理
	 *
	 * @access public
	 * @param $datetime 日時（YYYYMMDDHHNNSS）        	
	 * @return 結果 true：正常、false：異常
	 */
	public function validateTime($datetime) {
		// 日時なしか？
		if (! isset ( $datetime ) || empty ( $datetime )) {
			return false;
		}
		
		// 日時のフォーマット不正か？
		if (strlen ( $datetime ) !== 14) {
			return false;
		}
		
		// 日時データの取り出し
		$year = substr ( $datetime, 0, 4 );
		$month = substr ( $datetime, 4, 2 );
		$day = substr ( $datetime, 6, 2 );
		$hour = substr ( $datetime, 8, 2 );
		$min = substr ( $datetime, 10, 2 );
		$sec = substr ( $datetime, 12, 2 );
		$formattedDatetime = sprintf ( "%s-%s-%s %s:%s:%s", $year, $month, $day, $hour, $min, $sec );
		$appTimestamp = strtotime ( $formattedDatetime );
		$serverTimestamp = time ();
		
		$appAllowedMinusMinute = $appAllowedPlusMinute = Constant::API_TIME_MARGIN;
		
		$minTimestamp = $serverTimestamp - ($appAllowedMinusMinute * 60);
		$maxTimestamp = $serverTimestamp + ($appAllowedPlusMinute * 60);
		
		// パラメータ日時と、サーバー日時の差は、許容範囲か？
		if ($minTimestamp < $appTimestamp && $appTimestamp < $maxTimestamp) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * 送信用トークン取得（ユーザー登録前：SHA265でハッシュ）
	 *
	 * @param $did 端末ID        	
	 * @param $datetime 日時        	
	 * @return ハッシュ文字列
	 */
	public function getTokenInitial($did, $datetime) {
		$seed = sprintf ( "%s:%s:%s", Constant::APP_TOKEN_SEED, $did, $datetime );
		return hash ( 'sha256', $seed ); // SHA265でハッシュ化
	}
	
	/**
	 * 送信用トークン取得（ユーザー登録後：SHA265でハッシュ）
	 *
	 * @param $userId ユーザーID        	
	 * @param $installKey インストール固有キー        	
	 * @param $datetime 日時        	
	 * @return ハッシュ文字列
	 *
	 */
	public function getToken($userId, $installKey, $datetime) {
		$seed = sprintf ( "%s:%s:%s:%s", Constant::APP_TOKEN_SEED, $installKey, $userId, $datetime );
		return hash ( 'sha256', $seed ); // SHA265でハッシュ化
	}
	
	/**
	 * 送信用トークン（ユーザー登録前）のチェック
	 *
	 * @param s $token
	 *        	送信用トークン
	 * @param s $did
	 *        	端末固有ID（did）
	 * @param s $datetime
	 *        	アプリ送出時刻
	 * @return 結果 true：正常、false：不正
	 *        
	 */
	public function validateTokenInitial($token, $did, $datetime) {
		$serverToken = $this->getTokenInitial ( $did, $datetime );
		if ($serverToken === $token) { // トークンは正常か？
			return true;
		}
		return false;
	}
	
	/**
	 * 送信用トークン（ユーザー登録後）のチェック
	 *
	 * @param s $token
	 *        	送信用トークン
	 * @param s $userId
	 *        	ユーザーID
	 * @param s $datetime
	 *        	アプリ送出時刻
	 * @return 結果 true：正常、false：不正
	 */
	public function validateToken($token, $userId, $installKey, $datetime) {
		$serverToken = $this->getToken ( intval ( $userId ), $installKey, $datetime );
		if ($serverToken === $token) {
			return true;
		}
		return false;
	}
	
	/**
	 * 受信用トークン取得（ユーザー登録前/後：SHA265でハッシュ）
	 *
	 * @param $did 端末ID        	
	 * @param $datetime 日時        	
	 * @return ハッシュ文字列
	 *
	 */
	public function getReceiveToken($did, $datetime) {
		$seed = sprintf ( "%s:%s:%s", Constant::APP_TOKEN_SEED, $did, $datetime );
		return hash ( 'sha256', $seed ); // SHA265でハッシュ化
	}
	
	/**
	 * PKCS#5パディング（暗号化用）
	 *
	 * @access public
	 * @param $text 対象文字列        	
	 * @param $blocksize サイズ        	
	 * @return 結果
	 */
	private function pkcs5_pad($text, $blocksize) {
		$pad = $blocksize - (strlen ( $text ) % $blocksize);
		return $text . str_repeat ( chr ( $pad ), $pad );
	}
	
	/**
	 * PKCS#5パディング（復号化用）
	 *
	 * @access public
	 * @param $text 対象文字列        	
	 * @return 結果
	 */
	private function pkcs5_unpad($text) {
		$pad = ord ( $text {strlen ( $text ) - 1} );
		if ($pad > strlen ( $text ))
			return false;
		if (strspn ( $text, chr ( $pad ), strlen ( $text ) - $pad ) != $pad)
			return false;
		return substr ( $text, 0, - 1 * $pad );
	}
	
	/**
	 * 暗号化処理
	 *
	 * @param s $target
	 *        	暗号化対象の文字列
	 * @param s $data
	 *        	パラメータデータの配列
	 * @return 暗号化された結果配列 [0]：16進文字列化された暗号化文字列
	 *         [1]：16進文字列化されたIV
	 */
	public function encrypt($target, $data = null) {
		$resource = mcrypt_module_open ( Constant::CRYPT_LV, '', 'cbc', '' );
		if ($resource === false) { // mcrypt_module_openエラーか？
			return false;
		}
		
		$blockSize = mcrypt_enc_get_block_size ( $resource );
		
		// 初期化ベクター生成
		$iv = mcrypt_create_iv ( $blockSize, MCRYPT_RAND );
		
		// パディング
		$paddedStr = $this->pkcs5_pad ( $target, $blockSize );
		mcrypt_generic_init ( $resource, md5 ( Constant::SECRET_KEY ), $iv );
		
		// パラメータの暗号化（data部分）
		if ($data != null) {
			$paddedDataStr = $this->pkcs5_pad ( $data, $blockSize ); // パディング
			$encryptedData = mcrypt_generic ( $resource, $paddedDataStr ); // 暗号化
			                                                            
			// 初期化（これをしないと次のuser_idが復号失敗する）
			mcrypt_generic_init ( $resource, md5 ( Constant::SECRET_KEY ), $iv );
		}
		
		// 暗号化
		$encrypted = mcrypt_generic ( $resource, $paddedStr );
		mcrypt_generic_deinit ( $resource );
		mcrypt_module_close ( $resource );
		
		// 16進文字列化
		$hexEncStr = bin2hex ( $encrypted );
		$hexIvStr = bin2hex ( $iv );
		$result = array ();
		
		if ($data != null) {
			$hexEnDataStr = bin2hex ( $encryptedData );
			$result ["hex_data_str"] = $hexEnDataStr;
		}
		
		$result ["hex_enc_str"] = $hexEncStr; // 16進文字列化された暗号化文字列
		$result ["hex_iv_str"] = $hexIvStr; // 16進文字列化されたIV
		return $result;
	}
	
	/**
	 * 復号化メソッド
	 *
	 * @param
	 *        	s hexEncStr 暗号化された16進文字列
	 * @param
	 *        	s hexIvStr 16進文字列化された初期化ベクター
	 * @return 複合化された文字列
	 *
	 */
	public function decrypt($hexEncStr, $hexIvStr) {
		if (empty ( $hexEncStr ) || empty ( $hexIvStr )) { // 暗号化文字なしか？
			return false;
		}
		
		// バイナリ文字列にパック
		$data = pack ( "H*", $hexEncStr );
		$iv = pack ( "H*", $hexIvStr );
		
		// 復号化
		$decrypted = mcrypt_decrypt ( Constant::CRYPT_LV, md5 ( Constant::SECRET_KEY ), $data, MCRYPT_MODE_CBC, $iv );
		return $this->pkcs5_unpad ( $decrypted );
	}
	/**
	 * 暗号化された端末固有ID（did）を復号化
	 *
	 * @param
	 *        	s encryptedDid 暗号化された端末固有ID（did）
	 * @param
	 *        	s iv 初期化ベクター
	 * @return 復号化された端末固有ID（did）
	 */
	public function getDid($encryptedDid, $iv) {
		if (! $encryptedDid) { // didなしか？
			$this->_msg .= "did param not found.";
			return false;
		}
		return $this->decrypt ( $encryptedDid, $iv );
	}
	/**
	 * dataパラメータの復号化メソッド
	 *
	 * @param
	 *        	s data 暗号化されたdataパラメータ
	 * @param
	 *        	s hexIvStr 16進文字列化された初期化ベクター
	 * @return 複合化されたデータの配列
	 */
	public function getDecryptData($encryptedData, $hexIvStr) {
		if (empty ( $encryptedData ) || empty ( $hexIvStr )) { // 暗号化文字なしか？
			$this->_msg .= "data decrypt param not enough.";
			return false;
		}
		
		// バイナリ文字列にパック
		$data = pack ( "H*", $encryptedData );
		$iv = pack ( "H*", $hexIvStr );
		
		// 復号化
		$decrypted = mcrypt_decrypt ( Constant::CRYPT_LV, md5 ( Constant::SECRET_KEY ), $data, MCRYPT_MODE_CBC, $iv );
		$data = $this->pkcs5_unpad ( $decrypted );
		
		// URLデコード
		$data = urldecode ( $data );
		
		return $data;
	}
}
