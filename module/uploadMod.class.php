<?php
class uploadMod extends commonMod {

	public function index(){
		$data_return_array = array ();
		$files=$_FILES;
		//var_dump($_FILES);
		$ban_ext=array('php','asp','asp','html','htm','js','shtml','txt','aspx');
		if(!empty($files)){
			foreach($files as $file) {
				$name=$file['name'];
				$ext=explode('.', $file['name']);
				$ext=end($ext);
				if(in_array($ext, $ban_ext)){
					$data_return_array ['result'] = "0";
					$data_return_array ['msg'] = "非法文件格式";
					$data_return_array ['data'] = "";
						
					$data_return = $this->JSON ( $data_return_array );
					die ( $data_return );
					exit ();
				}
			}
		}else{
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "请选择上传文件";
			$data_return_array ['data'] = "";
				
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}

		//文件路径
		$file_path = __ROOTDIR__ . '/upload/';
		//文件URL路径
		$file_url = __ROOTDIR__ . '/upload/';
		//文件目录时间
		//$filetime=date('Y-m').'/'.date('d');
		$filetime='';
		//重命名
		function filename(){
			foreach($_FILES as $file) {
				$name=explode('.', $file['name']);
				$ext=end($name);
				$name=$name[0];
			}
			$pinyin = new Pinyin();
			$pattern = '/[^\x{4e00}-\x{9fa5}\d\w]+/u';
			$name = preg_replace($pattern, '', $name);
			$name = substr($pinyin->output($name, true),0,80);
			$name = time();
			if(file_exists(__ROOTDIR__.'/upload/'.$name.'.'.$ext)){
				$rand='-'.substr(cp_uniqid(),-5);
			}
			return $name.$rand;
		}

		//上传
		$upload = new UploadFile();
		$upload->maxSize = 1024 * 1024 * 2; //大小
		$upload->allowExts = explode(',', $this->config['ACCESSPRY_TYPE']); //格式
		$upload->savePath = $file_path . $filetime . '/'; //保存路径
		$upload->saveRule = 'filename'; //重命名

		if(!$upload->upload())
		{
	//echo ($upload->getErrorMsg()); //输出错误消息
	//            $this->error_msg($upload->getErrorMsg()); //输出错误消息
	//            return;
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "上传失败";
			$data_return_array ['data'] = "";
				
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}else{
			$info = $upload->getUploadFileInfo();
			$info = $info[0];
			//返回信息 Array ( [0] => Array ( [name] => 未命名.jpg [type] => image/pjpeg [size] => 53241 [key] => Filedata [extension] => jpg [savepath] => ../../../upload/2011-12-17/ [savename] => 1112170727041127335395.jpg ) )
			$ext=$info['extension'];

			 

			
			//设置高度和宽度
			
			$thumbwidth=$this->config['THUMBNAIL_MAXWIDTH'];
			
			$thumbheight=$this->config['THUMBNAIL_MAXHIGHT'];
			
			$thumb_cutout=intval(1);
			
			//过滤不支持格式进行缩图
			if($ext=='jpg'||$ext=='jpeg'||$ext=='png'||$ext=='gif'){
				$thumb=Image::thumb($file_path.$filetime.'/'.$info['savename'],$file_path.$filetime.'/thumb_'.$info['savename'],'',$thumbwidth,$thumbheight,'',$thumb_cutout);
			}
			

			//根据缩图返回数据
			if($thumb){
				$file=$filetime.'/thumb_'.$info['savename'];
			}else{
				$file= $filetime . '/'.$info['savename'];
			}
			$file= $filetime . '/'.$info['savename'];
			
			$title=str_replace('.'.$info['extension'],'', $info['name']);
			$json=array('error' => 0, 'url' =>$file, 'original'=>$file_url . $filetime . '/'.$info['savename'], 'file'=>$file,'title'=>$title,'ext'=>$ext,'msg'=>'成功');
			
			
			
			$data_return_array ['result'] = "1";
			$data_return_array ['msg'] = "";
			$data_return_array ['data']['url'] = $this->config['IMG_URL'].'upload'.$file;
				
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
			 
		}
	}
	
	public function test(){
		$this->display("upload");
	}
}
?>