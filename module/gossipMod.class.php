<?php
class gossipMod extends commonMod {
	public function __construct() {
		parent::__construct ();
	}

	public function create_user() {
		if (empty($_POST['nickname']) || empty($_POST['token'])) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "缺少参数";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$dec_token = module("SecurityUtil")->decrypt($_POST['token'],$_POST['']);


	}

	public function get_gossip_category_list() {
		$list = $this->model->table ( 'gossip_categories' )->where ( "status = 1" )->select ();
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $v ) {
				$tmp [$k] ['id'] = $v ['id'];
				$tmp [$k] ['name'] = $v ['name'];
				$img_list = $this->model->table ( 'gossip_images' )->where ( "category_id = " . $v ['id'] . " and status = 1" )->select ();
				if ($img_list) {
					$img_list_a = $this->i_array_column ( $img_list, "image" );
					$tmp [$k] ['images'] = $img_list_a;
					unset ( $img_list_a );
				} else {
					$tmp [$k] ['images'] = array ();
				}
				unset ( $img_list );
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['data'] ['categories'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function create_gossip() {
		$data ['message'] = text_in ( $_POST ['message'] );
		$data ['user_id'] = $_POST ['user_id'];
		$data ['parent_id'] = $_POST ['parent_id'];
		$data ['category_id'] = $_POST ['category_id'];
		$data ['font_id'] = $_POST ['font_id'];
		$data ['font_size'] = $_POST ['font_size'];
		$data ['text_direction'] = $_POST ['text_direction'];
		$data ['text_pos_x'] = $_POST ['text_pos_x'];
		$data ['text_pos_y'] = $_POST ['text_pos_y'];
		$data ['image'] = $_POST ['image'];
		$data ['secret'] = $_POST ['secret'];
		$data ['status'] = 1;
		$data ['created'] = date ( "Y-m-d H:i:s" );
		$data ['secret'] = date ( "Y-m-d H:i:s" );
		$this->model->table ( 'gossips' )->data ( $data )->insert ();
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = '';
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function upload_gossip_image() {
		if (empty($_POST['id'])) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "缺少参数";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$this->model->table ( 'gossips' )->data ( array (
			"image" => $_POST ['image']
			) )->where ( "id = " . $_POST ['id'] )->update ();
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = '';
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function get_gossip_list() {
		if (empty ( $_POST ['offset'] )) {
			$_POST ['offset'] = 0;
		}
		$where = " status ='1' ";
		if ($_POST ['category_id']) {
			$where .= " and category_id ='" . $_POST ['category_id'] . "' ";
		}
		if ($_POST ['parent_id']) {
			$where .= " and parent_id ='" . $_POST ['parent_id'] . "' ";
		}
		if ($_POST ['friends']) {
			$friends =  $list=$this->model->table('gossip_friends')->where("user_id = ".$_POST['user_id']." and status = 1")->select();
			if($friends){
				$friends_ids = $this->i_array_column(friends,"friend_id");
				$friend_id = implode(",", friends_ids);
				$where .=" and user_id in (".$friend_id.") ";
			}else{
				$where = "null";
			}
		}
		if ($where =='null') {
			$list = array();
		}else{
			$list = $this->model->table ( 'gossips' )->where ( $where )->limit ( $_POST ['offset'] . "," . $_POST ['limit'] )->select ();
		}
		$tmp = array ();
		if ($list) {
			foreach ( $list as $k => $v ) {
				unset ( $list [$k] ['status'] );
				unset ( $list [$k] ['created'] );
				unset ( $list [$k] ['modified'] );
				$list [$k] ['message'] = text_out ( $v ['message'] );
				$list [$k] ['comment_num'] = $this->model->table ( 'gossips' )->where ( "parent_id = " . $v ['id'] )->count ();
				$list [$k] ['fav_num'] = $this->model->table ( 'gossip_good' )->where ( "gossip_id = " . $v ['id'] )->count ();
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['data'] ['items'] = $list;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function get_gossip() {
		if (empty($_POST['id'])) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "缺少参数";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$info = $this->model->table ( 'gossips' )->where ( "id = " . $_POST ['id'] )->find ();
		unset ( $info ['status'] );
		unset ( $info ['created'] );
		unset ( $info ['modified'] );
		$info ['message'] = text_out ( $v ['message'] );
		$info ['comment_num'] = $this->model->table ( 'gossips' )->where ( "parent_id = " . $info ['id'] )->count ();
		$info ['fav_num'] = $this->model->table ( 'gossip_good' )->where ( "gossip_id = " . $info ['id'] )->count ();
		$data_return_array ['result'] = "1";
		$data_return_array ['data'] = $info;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
	public function update_fav() {
		if (empty($_POST['id']) || empty($_POST['user_id'])) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "缺少参数";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		if ($_POST ['status'] == '0') {
			$this->model->table ( 'gossip_good' )->where ( "gossip_id = '" . $_POST ['id'] . "' and user_id = '" . $_POST ['user_id'] . "'" )->delete ();
		} else {
			$data ['gossip_id'] = $_POST ['id'];
			$data ['user_id'] = $_POST ['user_id'];
			$data ['created'] = date ( "Y-m-d H:i:s" );
			$this->model->table ( 'gossip_good' )->data ( $data )->insert ();
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['data'] = '';
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function request_friend() {
		if (empty($_POST['id']) || empty($_POST['user_id'])) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "缺少参数";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$where['friend_id'] = $_POST ['id'];
		$where['user_id'] = $_POST ['user_id'];
		$check =$this->model->table('gossip_friends')->where($where)->find();
		if ($check) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "不能重复申请";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data ['status'] == '0';
		$data ['friend_id'] = $_POST ['id'];
		$data ['user_id'] = $_POST ['user_id'];
		$data ['created'] = date ( "Y-m-d H:i:s" );
		$data ['modified'] = date ( "Y-m-d H:i:s" );
		$this->model->table ( 'gossip_friends' )->data ( $data )->insert ();
		$data_return_array ['result'] = "1";
		$data_return_array ['data'] = '';
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function accept_friend() {
		if (empty($_POST['id']) || empty($_POST['user_id'])) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "缺少参数";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$where['friend_id'] = $_POST ['user_id'];
		$where['user_id'] = $_POST ['id'];
		$check = $this->model->table('gossip_friends')->where($where)->find();
		if (!$check) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "申请不存在";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$this->model->table ( 'gossip_friends' )->data ( array (
			"status" => 1,
			"modified"=>date ( "Y-m-d H:i:s" )
			) )->where ( $where )->update ();
		$info=$this->model->table('gossip_friends')->where("friend_id = ".$_POST ['id']." and user_id = ".$_POST ['user_id'])->find();
		if (empty($info)) {
			$data ['status'] == '1';
			$data ['friend_id'] = $_POST ['id'];
			$data ['user_id'] = $_POST ['user_id'];
			$data ['created'] = date ( "Y-m-d H:i:s" );
			$data ['modified'] = date ( "Y-m-d H:i:s" );
			$this->model->table ( 'gossip_friends' )->data ( $data )->insert ();
		}else{

			$this->model->table('gossip_friends')->data(array("status"=>1))->where("id = ".$info['id'])->update();
		}
		$data_return_array ['result'] = 1;
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = '';
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function unlock_gossip() {
		if (empty($_POST['id']) || empty($_POST['user_id'])) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "缺少参数";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$check = $this->model->table ( 'gossip_locks' )->where ( "user_id = " . $_POST['user_id'] . " and gossip_id = " . $_POST ['id'] )->find ();
		if (empty ( $check )) {
			$data ['user_id'] = $_POST['user_id'];
			$data ['gossip_id'] = $_POST ['id'];
			$data ['created'] = date ( "Y-m-d H:i:s" );
			$this->model->table ( 'gossip_locks' )->data ( $data )->insert ();
		}
		$data_return_array ['result'] = 1;
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = '';
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function get_unlock_status() {
		if (empty($_POST['id']) || empty($_POST['user_id'])) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "缺少参数";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$data_return_array = array ();
		$check = $this->model->table ( 'gossip_locks' )->where ( "user_id = " . $_POST['user_id'] . " and gossip_id = " . $_POST ['id'] )->find ();
		if (empty ( $check )) {
			$data ['status'] = 0;
		} else {
			$data ['status'] = 1;
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $data;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function get_present_list() {
		$data_return_array = array ();
		$list = $this->model->table ( 'gossip_presents' )->order ( 'id desc' )->select ();
	}

	public function get_news_list() {
		$data_return_array = array ();
		$list = $this->model->table ( 'trn_gossip_news' )->order ( 'id desc' )->where ( "user_id = " . $_POST['user_id'] )->select ();
		$tmp = array();
		if ($list) {
			foreach ($list as $key => $value) {
				if ($key=='0') {
					$last_updated = $value['modified'];
				}
				$tmp[$key]['title'] = $value['title'];
				$tmp[$key]['message'] = $value['message'];
				$tmp[$key]['updated'] = $value['modified'];
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data']['last_updated'] = $last_updated;
		$data_return_array ['data']['items'] = $data;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function get_user(){
		$data_return_array = array ();
		if (empty($_POST['id']) || empty($_POST['user_id'])) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "缺少参数";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}

		$user =  $info=$this->model->table('users')->where("id = ".$_POST['id'])->find();

		if (empty($user)) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "用户不存在";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}
		$tmp = array();
		$tmp['nickname'] = $user['nickname'];
		$tmp['image'] = $user['profile_image_url'];
		$friend =  $info=$this->model->table('gossip_friends')->where("user_id = ".$_POST['user_id']." and friend_id = ".$_POST['id'])->find();
		if (empty($friend)) {
			$tmp['friend_status'] = null;
		}else{
			$tmp['friend_status'] = $friend['status'];
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}

	public function get_friend_list(){
		$data_return_array = array ();
		if (empty($_POST['user_id'])) {
			$data_return_array ['result'] = "0";
			$data_return_array ['msg'] = "缺少参数";
			$data_return_array ['data'] = '';
			$data_return = $this->JSON ( $data_return_array );
			die ( $data_return );
			exit ();
		}

		$sql="
		SELECT A.*,B.nickname,B.profile_image_url
		FROM {$this->model->pre}friends A
		LEFT JOIN {$this->model->pre}users B ON A.friend_id = B.id
		WHERE A.user_id =".$_POST['user_id'];
		$list = $this->model->query($sql);
		$tmp = array();
		if ($list) {
			foreach ($list as $key => $value) {
				$tmp[$key]['id'] = $value['friend_id'];
				$tmp[$key]['nickname'] = $value['nickname'];
				$tmp[$key]['image'] = $value['profile_image_url'];
				$tmp[$key]['friend_status'] = $value['status'];
			}
		}
		$data_return_array ['result'] = "1";
		$data_return_array ['msg'] = "";
		$data_return_array ['data'] = $tmp;
		$data_return = $this->JSON ( $data_return_array );
		die ( $data_return );
		exit ();
	}
}
?>